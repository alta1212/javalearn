import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * day2
 */
public class day2 {
    public static void main(String[] args) {

        Scanner a = new Scanner(System.in);
        System.out.println("How many task you want to create ?");
        int taskCount = a.nextInt();
        String[] tasklist = new String[taskCount];
        String[] TaskAddList = {};
        String task;
        for (int i = 0; i < taskCount; i++) {

            do {
                System.out.println("Task num:" + (i + 1));
                task = a.next();
                tasklist[i] = task;

            } while (task.trim() == "");
        }
        // task 1

        System.out.println("Do you want add some task ? (y/n)");
        String cf = a.next();
        if (cf.toLowerCase().equals("y")) {
            System.out.println("How many task you want to add ?");
            int taskAddCount = a.nextInt();
            String taskAdd;
            TaskAddList = Arrays.copyOf(tasklist, tasklist.length + taskAddCount);
            for (int i = 0; i < taskAddCount; i++) {

                do {
                    System.out.println("Task add num:" + (i + 1));
                    taskAdd = a.next();
                    TaskAddList[tasklist.length + i] = taskAdd;

                } while (taskAdd.trim() == "");
            }

        }
        String[] select = tasklist;
        if (cf.equals("y")) {
            select = TaskAddList;
        }
        System.out.println("\n");
        // end task 1
        System.out.println("Task list :");
        for (int i = 0; i < select.length; i++) {
            System.out.println("Task " + (i + 1) + " : " + select[i]);
        }
        // Task 2
        System.out.println("Do you want to update any task ? (y/n)");
        if (a.next().toLowerCase().equals("y")) {
            System.out.println("Which task you want to update ?(1-" + select.length + ")");
            int ind = a.nextInt();
            System.out.println("Enter new task job :");
            String newtask = a.next();
            select[ind - 1] = newtask;

            System.out.println("Task list after update task num " + ind);
            for (int i = 0; i < select.length; i++) {
                System.out.println("Task " + (i + 1) + " : " + select[i]);
            }
        }

        // end task 2
        // task 3
        System.out.println("Do you want to delete any task ? (y/n)");
        if (a.next().toLowerCase().equals("y")) {
            String[] s = new String[select.length - 1];
            System.out.println("Which task you want to delete ?(1-" + select.length + ")");
            int ind = a.nextInt();
            int inde = 0;
            for (int i = 0; i < select.length; i++) {

                if (i != ind - 1) {
                    s[inde] = select[i];
                    inde++;
                }

            }
            select = s;
            System.out.println("Task list after delete task num " + ind);
            for (int i = 0; i < select.length; i++) {
                System.out.println("Task " + (i + 1) + " : " + select[i]);
            }
        }
        // end task 3
        // task 4

        System.out.println("Do you want to searh any task ? (y/n)");
        if (a.next().toLowerCase().equals("y")) {
            System.out.println("Enter keyword :");
            String word = a.next();
            System.out.println("Result:");
            for (String string : select) {
                if (string.contains(word))
                    System.out.println(string);
            }
        }
        // end task 4
        a.close();

    }

}