import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

/**
 * alta
 */
public class day1 {

    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);
        System.out.println("Insert your name");
        String name = a.next();
        System.out.println("Your name : " + name);
        System.out.println("How many task you want to create ?");
        int taskCount = a.nextInt();
        String[] tasklist = new String[taskCount];
        String task;
        for (int i = 0; i < taskCount; i++) {

            do {
                System.out.println("Task num:" + (i + 1));
                task = a.next();
                tasklist[i] = task;

            } while (task.trim() == "");
        }
        System.out.println("\n");
        Arrays.sort(tasklist);
        System.out.println("Increasing :");
        for (String taskString : tasklist) {
            System.out.println("Task :" + taskString);
        }
        System.out.println("\n");
        System.out.println("Decreasing :");
        for (int i = tasklist.length - 1; i >= 0; i--) {
            System.out.println("Task :" + tasklist[i]);
        }
        System.out.println("repeat :\n");
        for (int i = 0; i < tasklist.length; i++) {
            for (int j = i + 1; j < tasklist.length; j++) {
                if (tasklist[j] != null && tasklist[i] != null)
                    if (tasklist[i].equals(tasklist[j])) {
                        System.out.println(tasklist[j]);
                        tasklist[j] = null;
                        tasklist[i] = null;
                    }

            }
        }
        a.close();

    }
}