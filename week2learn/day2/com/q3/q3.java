import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;
import java.util.TreeSet;

public class q3 {
    //create tree set object
    static TreeSet<String> ts = new TreeSet<>(); 
    static void menu()
    {
     
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option :");
        System.out.println("1.Add element:");
        System.out.println("2.View all element :");
        System.out.println("3.View first element:");
        System.out.println("4.View last element:");
        System.out.println("5.Remove last element:");
        System.out.println("6.Get size:");
        System.out.println("7.View all element by iterator interface:");

        System.out.println("0.Exit app :");
        String key = sc.next();
        treeSet t = new treeSet();
        switch (key) {
            case "1":
             
                t.addElemnt(ts);
                menu();
                break;
            case "2":
                t.viewAll(ts);
                menu();
                break;
            case "3":
                t.viewFirst(ts);
                menu();
                break;
            case "4":
                t.viewLast(ts);
                menu();
                break;
        
            case "5":
                t.RemoveLast(ts);
                menu();
                break;
            case "6":
                t.getSize(ts);
                menu();
                break;

            case "7":
                t.viewByIteratorInterface(ts);
                menu();
                break;
            case "0":
                System.exit(0);
                menu();
                break;
            default: 
                menu();
                break;
        }
        
        sc.close();

    }
    public static void main(String[] args) {
        menu();
    }
}
