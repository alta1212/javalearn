import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class q2 {
    static Deque<Integer> deque = new ArrayDeque<Integer>();
    static void menu()
    {
     
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option :");
        System.out.println("1.Add element:");
        System.out.println("2.Insert to head:");
        System.out.println("3.Insert to tail:");
        System.out.println("4.View all element :");
        System.out.println("5.View first element:");
        System.out.println("6.Delete all :");
        System.out.println("0.Exit app :");
        String key = sc.next();
        element t = new element();
        switch (key) {
            case "1":
             
                t.addElemnt(deque);
                menu();
                break;
            case "2":
             
                t.addToHead(deque);
                menu();
                break;
            case "3":
                t.addToTail(deque);
                menu();
                break;
            case "4":
                t.viewAll(deque);
                menu();
                break;
        
            case "5":
                t.viewFirst(deque);
                menu();
                break;
            case "6":
                t.clear(deque);
                menu();
                break;
          
            case "0":
                System.exit(0);
                menu();
                break;
            default: 
                menu();
                break;
        }
        
        sc.close();

    }
    public static void main(String[] args) {
        menu();
    }
}
