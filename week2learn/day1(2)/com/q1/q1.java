
import java.util.ArrayList;
import java.util.Scanner;

public class q1 {
    public  ArrayList<arrayOBJ> arrTasks = new ArrayList<>();

    void menu()
    {
     
        Scanner sc = new Scanner(System.in);
        System.out.println("Choose option :");
        System.out.println("1.Add element:");
        System.out.println("2.Delete by index :");
        System.out.println("3.Delete by value :");
        System.out.println("4.View all element :");
        System.out.println("5.Print array size:");
        System.out.println("6.Print max value:");
        System.out.println("7.Print min value:");
        System.out.println("0.Exit app :");
        String key = sc.next();
        arrayOBJ t = new arrayOBJ();
        switch (key) {
            case "1":
            System.out.print("\033[H\033[2J");
                t.addTask(arrTasks);
                menu();
                break;
            case "2":
            System.out.print("\033[H\033[2J");
                t.deleteIndex(arrTasks);
                menu();
                break;
            case "3":
            System.out.print("\033[H\033[2J");
                t.deleteValue(arrTasks);
                menu();
                break;
            case "4":
            System.out.print("\033[H\033[2J");
                t.viewTask(arrTasks);
                menu();
                break;
        
            case "5":
            System.out.print("\033[H\033[2J");
                System.out.println(arrTasks.size());
                menu();
                break;
            case "6":
            System.out.print("\033[H\033[2J");
                t.ViewMax(arrTasks);
                menu();
                break;
            case "7":
            System.out.print("\033[H\033[2J");
                t.ViewMin(arrTasks);
                menu();
                break;
            case "0":
                System.exit(0);
                menu();
                break;
            default:
                System.out.print("\033[H\033[2J");
                menu();
                break;
        }
        sc.close();

    }
    public static void main(String[] args) {
        q1 k= new q1();

         k.menu(); 
    }
}
